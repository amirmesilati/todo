<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

use App\Todo;
use App\User;

class TodoController 
extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all todos for user 1
        $id=Auth::id(); //simulating that user 1 is loggeed in
       // $todos = Todo::all();
        $todos = User::find($id)-> todos;
        
        return view('todos.index',['todos'=>$todos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo = new Todo ();
        $id=Auth::id() ; 
        $todo -> title = $request -> title ;
        $todo -> user_id = $id;
        $todo -> status=0;
        $todo -> save(); 
       return redirect('todos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todo::find($id);
        return view('todos.edit' , compact('todo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo = Todo::find($id);
        if(!$todo->user->id ==Auth::id()) return (redirect('todos'));
        $todo -> update($request->except(['_token']));
        if($request->ajax())
        {
            return Response::json(array('result' => 'success','status'=>$request->status),200); 
        }
        return redirect('todos');

        //$todo -> update($request->all());
       // return redirect('todos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $todo = Todo::find($id);
        if(!$todo->user->id ==Auth::id()) return (redirect('todos'));
        $todo ->delete();
        return redirect('todos');
    }
}
