<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    //database fields to be allwed for massive assigment
    protected $fillable =[
        'title','status'
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }
}
