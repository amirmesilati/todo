<h1> Your Book List: </h1>
<head>
    <style>
        table, th, td{border: 1px solid black;}
    </style>
<table>
    <tr>
        <th> ID </th>
        <th> Title </th>
        <th> Author </th>
    </tr>

    @foreach($books as $book)
    <tr>
        <td>{{$book->id}}</td>
        <td>{{$book->title}}</td>
        <td>{{$book->author}}</td>
    </tr>
    @endforeach


</table