<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
               [
               'name' => 'jack',
               'email' => 'jack2@gmail.com',
               'password' =>'12345678',
               'created_at' => date('Y-m-d G:i:s')
               ],
               [
                'name' => 'jack2',
                'email' => 'jack@gmail.com',
                'password' =>'12345678',
                'created_at' => date('Y-m-d G:i:s')
                ]
               
            
           ]);
    }
}
