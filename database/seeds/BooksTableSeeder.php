<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
        [
            [
                'id'=>1 ,
                'title'=> 'Harry Potter and the Deathly Hallows' ,
                'author'=>'J.K. Rowling' ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>2 ,
                'title'=> 'The Name of the Wind' ,
                'author'=> 'by Patrick Rothfuss',
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>3 , 
                'title'=> 'Clockwork Princess' ,
                'author'=> 'by Cassandra Clare ',
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>4 ,
                'title'=> 'The Way of Kings' ,
                'author'=> 'by Brandon Sanderson' , 
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>5 ,
                'title'=> 'The Wise Mans Fear' ,
                'author'=> 'by Patrick Rothfuss' ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ]
        ]
   );
    }
}
